PREFIX = /usr/local
BINDIR = $(PREFIX)/bin

all: getxkblayout

install: getxkblayout
	@install -v -d "$(BINDIR)/" && install -m 0755 -v "./getxkblayout" "$(BINDIR)/getxkblayout"

uninstall:
	@rm -vrf "$(BINDIR)/getxkblayout"

getxkblayout: getxkblayout.c
	gcc -I/usr/include getxkblayout.c -o getxkblayout -lX11 -lxkbfile

clean: getxkblayout
	rm getxkblayout

.PHONY: all install uninstall clean
